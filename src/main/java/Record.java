package spark;

/**
 *H vasiki class gia na ginoun oi ypologismoi p.x. apo8hkeush. Diavasma tou arxeiou kai to apo8hkeush se typou record olwn twn egraffwn.
 *Kathe eggrafh ena record.
 */


/**
 * A class to hold each record fo further manipulation
 * Used by map to create records from the input file
 */
public final class Record implements java.io.Serializable
{
	private String from;
	private String to;
	
	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}
	
	/**
	 * Used by writeAsHadoopFile to write the data
	 */
	@Override
	public String toString()  //Gia na grafoume sto Hadoop to metratepoume se string
	{
		return (this.from + " " + this.to);
	}
}