package spark;

import java.io.File;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

/*Area Tree einai to dentro to geniko
*/

/**
 * A tree with the available areas for partitioning.
 */
public class AreaTree
{
	// Holds the root node of the tree
	private AreaNode start;
	
	//Used in order to find the depth of a area node 
	private int depth;

	/**
	 * AreaTree Constructor
	 * Needs a path to the xml file with all the available areas
	 */
	public AreaTree(String XMLPath) throws ParserConfigurationException, SAXException, IOException{
		this.readTree(XMLPath);//Path tou xml gia to dentro
	}
	
	/**
	 * Public method to get a node by its code
	 */
	public AreaNode getNode(String code)
	{
		return _getNode(code, start); 
	}

	/**
	 * recursive method that runs through the tree from a specfic node and compares
	 * the codes until it finds the right one
	 */
	private AreaNode _getNode(String code, AreaNode node)
	{
		if (node.code.equals(code))  //Anadromika vriskoume ton komvo mesa sto dentro
			return node;

		if (node.subareas != null)
		{
			for (AreaNode n : node.subareas)
			{
				AreaNode ret = _getNode(code, n);
				if (ret != null)
					return ret;
			}
		}

		return null;
	}

	/**
	 * Recursive method to read all the sub-areas from an xml
	 */
	private AreaNode readAreaNode(Node node) 
	{
		AreaNode n = new AreaNode(node.getAttributes().getNamedItem("name").getNodeValue(),
								  node.getAttributes().getNamedItem("code").getNodeValue());

		List<AreaNode> list = new ArrayList<AreaNode>();

		NodeList nodeList = node.getChildNodes();
		for (int i = 0; i < nodeList.getLength(); i++)
		{
			Node currentNode = nodeList.item(i);
			if (currentNode.getNodeType() == Node.ELEMENT_NODE)
			{
				// calls this method for all the children which is Element
				list.add(readAreaNode(currentNode));
			}
		}
		if(list.size() == 0)
			list = null;

		n.subareas = list;
		return n;
	}
	
//H read tree diavazei olous tous komvous sthn seira kai tous stelnei sthn AreaNode gia na tous dhmiourghsei mesa sto dentro.
	
	/**
	 * Reads the an xml file and uses readAreaNode in order to read all areas
	 */
	private void readTree(String path) throws ParserConfigurationException, SAXException, IOException
	{
		Path pt=new Path(path);
		FileSystem fs = pt.getFileSystem(new Configuration());
		
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
		Document document = docBuilder.parse(fs.open(pt));
		this.start = readAreaNode(document.getDocumentElement());
	}
	
	/**
	 * Prints the area tree (mostly for debugging...)
	 */
	public void printTree()
	{
		System.out.println("=================================================");
		System.out.println("Areas Tree");
		System.out.println("=================================================");
		this.depth = 0;
		PrintAll(start);
		System.out.println("=================================================");
	}

	/**
	 * Recursively prints an areas info
	 */
	private void PrintAll(AreaNode node)
	{
		System.out.println(++depth + ". " + node.area + " - " + node.code);
		if (node.subareas != null)
		{
			for (AreaNode n : node.subareas)
				PrintAll(n);
			--depth;
		}
	}

	/**
	 * Returns the depth of a specific node
	 */
	public int getDepth(AreaNode node)
	{
		return _getDepth(start, node.code, 0);
	}
	
	/**
	 * Calculates recursively the depth of a node
	 */
	private int _getDepth(AreaNode node, String code, int depth)
	{
		if (node == null)
			return 0;

		if (node.code.equals(code))
			return depth;

		int downDepth = 0;

		if (node.subareas == null)
			return 0;

		for (AreaNode n : node.subareas)
		{
			downDepth = _getDepth(n, code, depth + 1);
			if (downDepth != 0)
				return downDepth;
		}

		return downDepth;
	}

	//Dinoume ena komvo kai girnaei ta paidia tou gia na parei o partitioner kai na xrhsimopoieisei
	/**
	 * Returns a list with the sub-areas of a node  
	 */
	public List<AreaNode> getChildren(AreaNode node)
	{
		return this.getNode(node.code).subareas;
	}
	
	/**
	 * Returns a list with all the areas codes
	 */
	public List<String> getAllCodes()
	{
		List<String> list = new ArrayList<String>();
		_getAllCodes(start, list);
		return list;
	}

	/**
	 * Recursively gets a list with all the codes
	 */
	private void _getAllCodes(AreaNode node, List<String> list)
	{
		list.add(node.code);
		
		if (node.subareas != null)
		{
			for (AreaNode n : node.subareas)
				_getAllCodes(n, list);
		}
	}
	
	public boolean areComparable(AreaNode first, AreaNode second)
	{
		//System.out.println("Comparing " + first.code + " with " + second.code);
		if(first.code == "" || second.code == "")
			return true;
	
		//System.out.println("Comparing " + first.code + " with " + second.code);
		
		List<String> areas = new ArrayList<String>();
		_getAllCodes(first, areas);
		//if(areas.contains(second.code)) System.out.println(second.code + " is a child of " + first.code);
		if(areas.contains(second.code)) return true;
		
		areas = new ArrayList<String>();
		_getAllCodes(second, areas);
		//if(areas.contains(first.code)) System.out.println(first.code + " is a child of " + second.code);
		if(areas.contains(first.code)) return true;
		
		return false;
	}
}
