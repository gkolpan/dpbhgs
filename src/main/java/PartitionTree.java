package spark;


import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.io.File;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.apache.spark.SparkConf;
import org.apache.spark.Accumulator;
import org.apache.spark.api.java.*;
import org.apache.spark.storage.StorageLevel;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.HashPartitioner;
import scala.Tuple2;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.lang.Math;

/**
 * A tree with all the partitions
 * (internally represented as List)
 */
public class PartitionTree
{
	//The list with current partitions
	List<PartitionNode> tree = new ArrayList<PartitionNode>();

	//A list with partitions that need to be split!
	List<PartitionNode> toBeSplit = new ArrayList<PartitionNode>();

	//The area tree to use in order to perform the splits
	AreaTree areas;

	//The xml to read-write the partition
	private String xmlPath;
	
	//Spark configuration
	private SparkConf conf = new SparkConf().setAppName("Test Call Data");
	private JavaSparkContext sc = new JavaSparkContext(conf);

	
	public double getDistance()
	{
	double sum = 0;
		try{
			for(PartitionNode node : tree)
				sum += Math.pow((double) (node.getFileSize() - Parameters.SplitSize), (double) 2);
		} catch (IOException ex){}
		return Math.sqrt(sum);
	}
	
	/**
	 * PartitionTree Constructor
	 * Needs a path to the xml file that will read-write the partition information
	 */
	 
	public PartitionTree(AreaTree tree, String XMLPath) throws ParserConfigurationException, SAXException, IOException
	{
		this.areas = tree;
		this.xmlPath = XMLPath;
		this.ReadTree();
	}

	/**
	 * Saves the tree to the xml
	 * xml is destroyed and rewritten 
	 */
	private void SaveTree() throws ParserConfigurationException, TransformerException, IOException
	{
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

		// root elements
		Document doc = docBuilder.newDocument();
		Element rootElement = doc.createElement("partitions");
		doc.appendChild(rootElement);

		for (PartitionNode partitionNode : tree)
		{
			// partition element
			Element partition = doc.createElement("partition");
			rootElement.appendChild(partition);

			// from element
			Element from = doc.createElement("from");
			from.appendChild(doc.createTextNode(partitionNode.from.code));
			partition.appendChild(from);

			// to element
			Element to = doc.createElement("to");
			to.appendChild(doc.createTextNode(partitionNode.to.code));
			partition.appendChild(to);
		}
		// write the content into xml file
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");

		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new StringWriter());
		transformer.transform(source, result);
		
		String xmlString = result.getWriter().toString();
		
		Path pt=new Path(xmlPath);
		FileSystem fs = pt.getFileSystem(new Configuration());
		
		if(fs.exists(pt)) fs.delete( pt, true );
		
		BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter( fs.create(pt), "UTF-8" ));
		bufferedWriter.write(xmlString);
		bufferedWriter.flush();
		bufferedWriter.close();
	}

	/**
	 * Reads the partition tree from the xml
	 */
	private void ReadTree() throws ParserConfigurationException, SAXException, IOException
	{
		Path pt=new Path(xmlPath);
		FileSystem fs = pt.getFileSystem(new Configuration());
		
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fs.open(pt));

		doc.getDocumentElement().normalize();
		NodeList nList = doc.getElementsByTagName("partition");

		for (int temp = 0; temp < nList.getLength(); temp++)
		{
			Node nNode = nList.item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE)
			{
				Element eElement = (Element) nNode;
				String from = eElement.getElementsByTagName("from").item(0).getTextContent();
				String to = eElement.getElementsByTagName("to").item(0).getTextContent();

				tree.add(new PartitionNode(areas.getNode(from), areas.getNode(to)));
			}
		}
	}

	/**
	 * Print the partition tree
	 * (Mostrly for debugging purposes)
	 */
	public void printTree()
	{
		System.out.println("------ Current Partition Tree ------");
		for (PartitionNode p : tree)
			System.out.println("("+p.from.code + ") - (" + p.to.code + ")");
		System.out.println("------------------------------------");
	}

	/**
	 * Gets the number of partitions so far
	 */
	private int partitionSize()
	{
		return tree.size();
	}

	/**
	 * Reads data from the path and outputs to the specific partitions
	 * Check file sizes
	 * Splits if the partitions if necessary and recall InputData 
	 */
	public void InputData(String path) throws Exception
	{	
		System.out.println("-------------Starting Input Data-------------");
		long start = System.currentTimeMillis();
		System.out.println("Input path: " + path);
		this.printTree();
		
		Path tmp = new Path(Parameters.Output + "tmp");	
		FileSystem filesystem = tmp.getFileSystem(new Configuration());
		
		if(filesystem.exists(tmp))
			filesystem.delete(tmp, true);
		
		Path pt=new Path(path);
		FileSystem fs = pt.getFileSystem(new Configuration());
	
		List<String> froms = new ArrayList<String>();
		List<String> tos = new ArrayList<String>();
			
		for (PartitionNode p : tree)
		{
			froms.add(p.from.code);
			tos.add(p.to.code);
		}			
		long elapsedTimeMillis = System.currentTimeMillis()-start;
		
		float elapsedTimeSec = elapsedTimeMillis/1000F;
		System.out.println(elapsedTimeSec);
		System.out.println("Starting mapToPair");
		
		sc.textFile(path).mapToPair(line -> {
			String[] parts = line.split(" ");
			Record record = new Record();
			record.setFrom(parts[0]);
			record.setTo(parts[1]);
						
			String key = "";
			for (int i=0; i< froms.size(); ++ i)
			{
				if(record.getFrom().startsWith(froms.get(i)) && record.getTo().startsWith(tos.get(i)))
				{
					key = "(" + froms.get(i) + ")-(" + tos.get(i) + ")";
					break;
				}
			}
			if(key.equals(""))
				key = "undefined_values";
				
			return new Tuple2<String, Record>(key, record);
		})
		.partitionBy(new HashPartitioner(tree.size()))
		.saveAsHadoopFile(Parameters.Output + "tmp", String.class, String.class, RDDMultipleTextOutputFormat.class);
		
		System.out.println("Moving files to correct locations");
		
		Date curr_date = new Date(System.currentTimeMillis());
		DateFormat df = new SimpleDateFormat("yyyyMMdd-HHmmss.SSS");
		
		for(int i=0; i<froms.size();++i)
		{
			Path directory = new Path(Parameters.Output + "(" + froms.get(i) + ")-(" + tos.get(i) + ")");
			if(!filesystem.exists(directory))
					filesystem.mkdirs(directory);
					
			Path file = new Path(Parameters.Output + "tmp/(" + froms.get(i) + ")-(" + tos.get(i) + ")");
			Path output = new Path(Parameters.Output + "(" + froms.get(i) + ")-(" + tos.get(i) + ")/" + df.format(curr_date));
			
			filesystem.rename(file, output);
		}
		
		filesystem.delete(tmp, true);
		
		// edo oloklhronei ta spasimata
		// ara psaxnontas to directory mporoyme na diagrapsoyme ta palia partition
		// oti apo to direcory den einai sto partition tree mporei na ginei delete
		
		System.out.println("Checking sizes");
		// Check sizes
		for (PartitionNode p : tree)
		{
			if (p.getFileSize() > Parameters.SplitSize && !toBeSplit.contains(p))
				toBeSplit.add(p);
		}

		boolean split = false;
		System.out.println("--------------InputData Finished--------------");

		for (Iterator<PartitionNode> iter = toBeSplit.iterator(); iter.hasNext();)
		{
			
			PartitionNode p = iter.next();
			
			System.out.println("Splitting : " + p.toString());
			
			if (Parameters.SplitMethod.equals("roundrobin"))
				split = this.RoundRobinSplit(p);
			else if (Parameters.SplitMethod.equals("minsplit"))
				split = this.MinSplitPartition(p);
			else if (Parameters.SplitMethod.equals("querydriven"))
				split = this.QueryDriverPartition(p);
			else
			{
				System.out.println("Unknown split method... No split!");
				toBeSplit.clear();
				return;
			}
			
			
			iter.remove();

			if (split)
			{
				System.out.println("Rerunning InputData with input : " + p.path.toString() + "/");
				InputData(p.path.toString() + "/");
				
				pt = p.path;
				fs = pt.getFileSystem(new Configuration());
				if(fs.exists(pt)) fs.delete( pt, true );
			}
		}

		// If split is true then the tree has been updated!
		if (split)
			this.SaveTree();
		//*/
	}

	/**
	 * Roundrobin split algorithm
	 */
	private boolean RoundRobinSplit(PartitionNode node)
	{
		List<PartitionNode> parts = new ArrayList<PartitionNode>();
		List<AreaNode> ars;

		if ((areas.getDepth(node.from) < areas.getDepth(node.to) && areas.getChildren(node.from) != null)
		        || areas.getChildren(node.to) == null)
		{
			// System.out.println("Splitting Source");
			ars = areas.getChildren(node.from);

			if (ars == null)
				return false;

			for (AreaNode f : ars)
				parts.add(new PartitionNode(f, node.to));
		}
		else
		{
			// System.out.println("Splitting Destination");
			ars = areas.getChildren(node.to);

			if (ars == null)
				return false;

			for (AreaNode f : ars)
				parts.add(new PartitionNode(node.from, f));
		}

		tree.addAll(parts);
		System.out.println("Parts:");
		for(PartitionNode n : parts)
			System.out.println(n.toString());
		tree.remove(node);

		return true;
	}

	/**
	 * MinSplit partition algorithm
	 */
	private boolean MinSplitPartition(PartitionNode node)
	{
		List<PartitionNode> parts = new ArrayList<PartitionNode>();
		List<AreaNode> ars;

		if (areas.getChildren(node.to) == null
		        || (areas.getChildren(node.from) != null && areas.getChildren(node.from).size() < areas.getChildren(
		                node.to).size()))
		{
			// System.out.println("Splitting Source");
			ars = areas.getChildren(node.from);

			if (ars == null){
				System.out.println("skaw apo source");
				return false;
			}
			for (AreaNode f : ars)
				parts.add(new PartitionNode(f, node.to));
		}
		else
		{
			// System.out.println("Splitting Destination");
			ars = areas.getChildren(node.to);

			if (ars == null){
				System.out.println("skaw apo destination");
				return false;
			}
			for (AreaNode f : ars)
				parts.add(new PartitionNode(node.from, f));
		}

		tree.addAll(parts);
		tree.remove(node);

		return true;
	}
	
	/**
	 * Query-Driven partition algorithm
	 */
	private boolean QueryDriverPartition(PartitionNode node) throws Exception
	{
		List<PartitionNode> parts = new ArrayList<PartitionNode>();
		List<String> queries = new ArrayList<String>();
		queries.add("23-2294");
		queries.add("21-21");
		queries.add("21-23");
		queries.add("227-21");
		queries.add("23-21");
		queries.add("2373-2391");
		queries.add("2381-2382");
		queries.add("239-2494");
		queries.add("25-25");
		
		long parentSize = node.getFileSize();
		List<AreaNode> ars;
		List<PartitionNode> froms = new ArrayList<PartitionNode>();
		List<PartitionNode> tos = new ArrayList<PartitionNode>();
		
		double fromOverHead = -1, toOverHead = -1;
		
		// Calculate overhead in case of splitting from
		ars = node.from.subareas;
		//Must have children... daaah!
		if(ars != null){
			for (AreaNode f : ars)
					froms.add(new PartitionNode(f, node.to));
			
			fromOverHead = calculateOverHead(froms, queries, parentSize);
			System.out.println("Calculated fromOverHead: " + fromOverHead);
		}
		// Calculate overhead in case of splitting to
		
		ars = node.to.subareas;
		//Must have children... daaah!
		if(ars != null){
			for (AreaNode f : ars)
					tos.add(new PartitionNode(node.from, f));
			
			toOverHead = calculateOverHead(tos, queries, parentSize);
			System.out.println("Calculated toOverHead: " + toOverHead);
		}

		// Compare overheads
		if(fromOverHead == toOverHead){
			System.out.println("Going for round robin... (because I can!!!)");
			return this.RoundRobinSplit(node);
		}
			
		if(toOverHead == -1 || fromOverHead < toOverHead){
			tree.addAll(froms);
			System.out.println("Adding froms...");
		}else if(fromOverHead == -1 || fromOverHead > toOverHead){
			tree.addAll(tos);
			System.out.println("Adding tos...");
		}
		tree.remove(node);
		
		return true;
	}
	
	private double calculateFitness(PartitionNode node, String query)
	{
		AreaNode qFrom = areas.getNode(query.split("-")[0]);
		AreaNode qTo = areas.getNode(query.split("-")[1]);

		// Calculate From
		double fitnessFrom, fitnessTo;

		System.out.println("Calculating fitness for node " + node.toString() + " for " + qFrom.code.toString() + " and " + qTo.code.toString());
		
		if(!areas.areComparable(node.from, qFrom)){
//			System.out.println(node.from.code + " and " + qFrom.code + " are not comparable");
			fitnessFrom = 0.0;
		}else if(areas.getDepth(node.from) >= areas.getDepth(qFrom)){
//			System.out.println(node.from.code + " and " + qFrom.code + " are comparable and depth(" + node.from.code + ") > depth(" + qFrom.code + ")");
			fitnessFrom = 1.0;
		}else{
//			System.out.println(node.from.code + " and " + qFrom.code + " are comparable and depth(" + node.from.code + ") < depth(" + qFrom.code + ")");
			fitnessFrom = (double)(areas.getChildren(qFrom) != null ? areas.getChildren(qFrom).size() : 0) / (double)areas.getChildren(node.from).size();
		}

		//Calculate To
		if(!areas.areComparable(node.to, qTo))
			fitnessTo = 0.0;
		else if(areas.getDepth(node.to) >= areas.getDepth(qTo))
			fitnessTo = 1.0;
		else
			fitnessTo = (double)(areas.getChildren(qTo) != null ? areas.getChildren(qTo).size() : 0)  / (double)areas.getChildren(node.to).size();
		
		System.out.println("Fitness for node " + node.toString() + " for query " + query + " is: (from)" + fitnessFrom + " * " + fitnessTo + "(to) = " + (fitnessFrom * fitnessTo));
		return fitnessFrom * fitnessTo;
	}
	
	private double calculateOverHead(List<PartitionNode> nodes, List<String> queries, long parentSize)
	{
		double sum = 0;
		for(PartitionNode n : nodes)
		{
			double partFit = 0;
			for(String q : queries)
				partFit += calculateFitness(n, q);
			
			System.out.println("Sum of partition fitness: " + partFit);
			partFit /= (double)queries.size();
			System.out.println("Final partition fitness: " + partFit);
			
			sum += (1-partFit);
		}
		
		
		return ((double)parentSize / (double)nodes.size()) * sum;
	}
}
