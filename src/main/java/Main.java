package spark;

public class Main
{
	static int depth = 0;

	public static void main(String[] args) throws Exception
	{
		if(args.length != 1){
			System.out.println("Usage: program ParametersXML");
			System.exit(1);
		}
		
		//Initiate the parameters class (the static variables)
		new Parameters(args[0]);
		
		//Create the area tree
		AreaTree areas = new AreaTree(Parameters.Areas);
		//areas.printTree();
		
		//Create the partition tree
		PartitionTree partitions = new PartitionTree(areas, Parameters.Partitions);
		
		//Input data to the partition
		partitions.InputData(Parameters.Input);
		
		System.out.println("Distance: " + partitions.getDistance());
	}
}
